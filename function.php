<?php

function redirectionDashboard () {
    header('Location: index.php');
    die;
}

function redirection () {
    header('Location: login.php');
    die;
}
function recupcsv ($nomfichier) {
    $array = array();
    $recupfichier = fopen($nomfichier, "r");
    if($recupfichier) {
        while (!feof($recupfichier)) {
            $valeur = fgetcsv($recupfichier, 0 , ',' , '"' );
            $array [] = $valeur;
        }
        fclose($recupfichier);
    }
    return $array ;
}
